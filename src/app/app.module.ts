import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardService } from './services/dashboard.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SimulateLoginInterceptor } from './auth/simulate-login.interceptor';
import { SigninComponent } from './signin/signin.component';
import { FormsModule } from '@angular/forms';
import { LoginService } from './services/login.service';
import { fakeBackendProvider } from './auth';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MainComponent } from './main/main.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SigninComponent,
    SidebarComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    DashboardService,
    LoginService,
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: SimulateLoginInterceptor,
    //   multi: true
    // },

    fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
