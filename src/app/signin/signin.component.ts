import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { SimulateLoginInterceptor } from '../auth/simulate-login.interceptor';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  error = '';

  constructor(private loginService : LoginService, private router: Router) { }

  ngOnInit(): void {
  }

  OnSubmit(userName,password){
    this.loginService.signin(userName, password)
            .pipe(first())
            .subscribe(
                data => {
                  this.router.navigateByUrl('/dashboard')
                },
                error => {
                  this.error = error;
                  // this.router.navigateByUrl('/login')
                });
  }

}
