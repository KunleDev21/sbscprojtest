import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  public remoteServerBaseUrl: string = "https://609e799e33eed800179589ba.mockapi.io";

  constructor() { }
}
