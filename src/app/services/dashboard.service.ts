import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppService } from './app.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService extends AppService {

  posts: Object;
  selectedDriversList : DashboardListEditDto;
  constructor(private http: HttpClient) {
    super();
   }

  //  getAllDashboardData() : Observable<DashboardListDto[]>{
  //    return this.http.get<DashboardListDto[]>(this.remoteServerBaseUrl + '/v4/dashboard');
  //  }

   getDashboardById(Id: number){
    console.log("I got to service");
    return this.http.get(this.remoteServerBaseUrl + '/v4/dashboard') as Observable<DashboardListEditDto>
  }
}

export class DashboardListDto {
  id: number;
  logged_user : string;
  notification_count : string;
  // chart_stat : Date;
  // label : string;
  // capex : string;
  // spent : string;
  // balance : string;
}

export class DashboardListEditDto {
  id: number;
  logged_user : string;
  notification_count : string;
  chart_stat : [];
  label : string;
  capex : string;
  spent : string;
  // balance : string;
}
