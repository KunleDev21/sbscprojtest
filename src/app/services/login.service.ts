import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../environment/environment';
import { UserModel } from '../models/user'

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private CurrPersonSub : BehaviorSubject<UserModel>;
  public CurrPer : Observable<UserModel>;

  constructor(private http : HttpClient, public router : Router) 
  {
    this.CurrPersonSub = new BehaviorSubject<UserModel>(JSON.parse (localStorage.getItem('sbscUser')));
    this.CurrPer = this.CurrPersonSub.asObservable();
  }

  public get currUser(): UserModel {
    return this.CurrPersonSub.value;
  }

  signin(username: string, password: string){
    return this.http.post<any>(`${environment.apiUrl}/users/authenticate`, { username, password})
    .pipe(map(user => {
      localStorage.setItem('currentUser', JSON.stringify(user));
      this.CurrPersonSub.next(user);
      return user;
  }));
  }

  sigout() {
    localStorage.removeItem('sbscUser');
    this.router.navigateByUrl('/login')
    this.CurrPersonSub.next(null);
  }
}
