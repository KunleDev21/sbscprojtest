import { Component, OnInit } from '@angular/core';
import { DashboardListEditDto, DashboardService } from "../services/dashboard.service";
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  // getDashboardData: any;
  currDate = new Date();
  clickedTxt = false;
  cardTitle = "Annual Play Budget";
  clickedTxtTitle = false;
  clickedTxtBal = false;
  cardColor = "#fff7f5";

  public personnelDashboard: DashboardListEditDto = new DashboardListEditDto();

  constructor(public dashboardService : DashboardService, public logOut : LoginService) { }

  ngOnInit(): void {
    this.currDate;
    this.getTerminalData(1)
  }

  OnClick() {
    this.clickedTxt = true;
  }

  OnClickTitle() {
  this.clickedTxtTitle = true;
  }

  OnClickBalance() {
    this.clickedTxtBal = true;
  }

  OnClickChangeColor() {
    this.clickedTxtBal = true;
  }

  getTerminalData(id) {
    //this.spinner.show();
    this.dashboardService.getDashboardById(id).subscribe(result => {
      this.personnelDashboard = result[0];
      console.log(result[0]);
      //this.spinner.hide();
    })
  }

  OnClickLogOut(){
    this.logOut.sigout();
    console.log("logout reached");
  }


}
